﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gettingTrainConnectionsTest.db_model;
namespace gettingTrainConnectionsTest.controller
{


    class StationController
    {


        private TrainDbContext context { get; set; }

        public StationController()
        {
            context = new TrainDbContext();
        }

        public bool checkIfStationInDb(string station_name)
        {
            var stations =  context.Stations.ToList();
            foreach (var item in stations)
            {
                if (item.stationName == station_name)
                {
                    return true;
                }
            }
            return false;
        }

        public bool addStationToDb(string station_name)
        {
            if (!(this.checkIfStationInDb(station_name)))
            {
                context.Stations.Add(new dbStation(station_name));
                context.SaveChanges();
                Console.WriteLine("station added "+station_name);
                return true;
            }

            return false;
        }

       
    }
}
