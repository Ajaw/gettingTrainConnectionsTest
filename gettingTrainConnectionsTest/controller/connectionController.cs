﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gettingTrainConnectionsTest.db_model;
using gettingTrainConnectionsTest.model;
namespace gettingTrainConnectionsTest.controller
{
    class ConnectionController
    {

        private TrainDbContext context { get; set; }

        public ConnectionController()
        {
            context = new TrainDbContext();
            context.Configuration.LazyLoadingEnabled = false;
        }

        public int addConnectionToDb(connection _connection)
        {
            if (!(this.checkIfConnectionInDb(_connection)))
            {
                var connections = context.Connections;
                dbConnection _con_to_add = new dbConnection();
                List<dbStationsOnConnection> stopsOnRoute = new List<dbStationsOnConnection>();
                int count = 1;
                foreach (var item in _connection.stationOnRoute)
                {

                    stopsOnRoute.Add(createStopOnRouteFromStationModel(item, count));
                    count++;
                }
                _con_to_add.StopsOnRoute = stopsOnRoute;

                context.Connections.Add(_con_to_add);

                context.SaveChanges();
                return _con_to_add.ID;
            }
            return 0;
        }

        public dbConnection getConnectionByStationList(connection _connection)
        {
            var connections = context.Connections.Include("StopsOnRoute").Include("StopsOnRoute.station");
            station current_name = _connection.stationOnRoute.First();
            foreach (var item in connections)
            {
                
                if ((this.compareStations(_connection.stationOnRoute, item.StopsOnRoute.ToList())))
                {
                    return item;
                }
            }
            return null;
        }

        private dbStationsOnConnection createStopOnRouteFromStationModel(station _station,int number_on_route)
        {
            dbStationsOnConnection temp_station = new dbStationsOnConnection();
            temp_station.stationID = context.Stations.Where(a => a.stationName == _station.name).First().ID;
            temp_station.stationNumberOnRoute = number_on_route;
            return temp_station;
        }

        public bool checkIfConnectionInDb(connection _connection)
        {
            if (this.getConnectionByStationList(_connection)!=null)
            {
                return true;
            }
            return false;
        }

        private bool compareStations(List<station> target, List<dbStationsOnConnection> dbList)
        {
            if (target.Count != dbList.Count)
            {
                return false;
            }
            var sortedDb = dbList.OrderBy(a => a.stationNumberOnRoute).ToList();
            for (int i = 0; i < target.Count; i++)
            {
                if (!(target[i].name == sortedDb[i].station.stationName))
                {
                    return false;
                }
            }
            return true;
        }

        public dbConnection getConnectionById(int connectionId)
        {
            return context.Connections.Include("periodicConnectionList").Include("StopsOnRoute").Where(a => a.ID == connectionId).FirstOrDefault();
        }
        
    }
}
