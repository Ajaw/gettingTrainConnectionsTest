﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using gettingTrainConnectionsTest.db_model;
using gettingTrainConnectionsTest.model;
using gettingTrainConnectionsTest.db_model.connectionData;
namespace gettingTrainConnectionsTest.controller
{
    class stopsController
    {

        private TrainDbContext context { get; set; }

        public stopsController()
        {
            context = new TrainDbContext();
        }

        public void checkStopHours(connection _connection, List<HourlyRoute> routes)
        {
            ConnectionController contr = new ConnectionController();
          
            var connection = contr.getConnectionById(_connection.connectionDbId);
            var stops = connection.periodicConnectionList;
            var stopsOnRouteDb =  connection.periodicConnectionList.ToList();
            foreach (var item in routes)
            {
                compareDbStops(item, stopsOnRouteDb);
            }

           var no_to_remove =  routes.Where(a => a.alreadyInDB == true).Select(x => x.dbConnectionHourID);

           stopsOnRouteDb.RemoveAll(x => !no_to_remove.Contains(x.ID));

            var to_add = routes.Where(a => a.alreadyInDB == false);
            List<dbConnectionToHour> temp_list = new List<dbConnectionToHour>();
            foreach (var item in to_add)
            {
                dbConnectionToHour temp_hour = new dbConnectionToHour();
                
                stopsOnRouteDb.Add(temp_hour);
                temp_hour.ArrivalHours = new List<ArrivalHours>();
                context.SaveChanges();
                foreach (var hour in item.stopsOnRoute)
                {
                    ArrivalHours temp_stop = new ArrivalHours();                    
                    temp_stop.arrivalTime = hour.arrivalTime;
                    temp_stop.departureTime = hour.departureTime;
                    temp_stop.platform = hour.platform;
                    context.StopHours.Add(temp_stop);
                    temp_hour.ArrivalHours.Add(temp_stop);
                    
                }
                temp_hour.connectionID = _connection.connectionDbId;
                temp_hour.onWeekendConnection = false;
                context.StopsHoursToConnection.Add(temp_hour);
                temp_list.Add(temp_hour);
                context.SaveChanges();
            }
            connection.periodicConnectionList = temp_list;
           context.Connections.AddOrUpdate(connection);
            context.SaveChanges();

          //  context.SaveChanges();
           
           
        }

        private void compareDbStops(HourlyRoute dl_route,  List<dbConnectionToHour> db_routes)
        {
          
            foreach (var item in db_routes)
            {
                int i = 0;
                bool found_match = false;
                foreach (var hour in item.ArrivalHours)
                {
                    var temp_route = dl_route.stopsOnRoute[i];
                   
                    if (hour.arrivalTime.Hour == temp_route.arrivalTime.Hour && hour.arrivalTime.Minute ==temp_route.arrivalTime.Minute)
                    {
                        if (hour.departureTime.Hour == temp_route.departureTime.Hour && hour.departureTime.Minute == temp_route.departureTime.Minute)
                        {
                            found_match = true;
                        }
                        else
                        {
                            found_match = false;    
                        }
                    }
                    else
                    {
                        found_match = false;
                    }
                }
                if (found_match)
                {
                    dl_route.dbConnectionHourID = item.ID;
                    dl_route.alreadyInDB = true;
                    break;
                }
            }
            dl_route.alreadyInDB = false;
        }
    }
}
