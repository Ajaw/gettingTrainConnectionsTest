﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.connectionData
{
    class dbConnectionToHour
    {
        public int ID { get; set; }

        public int connectionID { get; set; }
        public virtual dbConnection connection { get; set; }

        public string trainLabel { get; set; }


        public ICollection<ArrivalHours> ArrivalHours { get; set; }

        public bool onWeekendConnection { get; set; }

        public virtual dbScheduleVersion version { get; set; }
    }
}
