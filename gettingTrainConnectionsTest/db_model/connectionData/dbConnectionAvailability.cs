﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.connectionData
{
    class dbConnectionAvailability
    {

        public int id { get; set; }

        public DateTime beggingDate { get; set; }
        public DateTime endDate { get; set; }
        public bool ignoreWeekDays { get; set; }

     
        public virtual dbConnectionMapping mapping { get; set; }

    }
}
