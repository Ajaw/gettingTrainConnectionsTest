﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.connectionData
{
    class dbConnectedStation
    {
        public int id { get; set; }

        public int motherStationID { get; set; }

        public int connectedStationID { get; set; }

        public int previousStationID { get; set; }

        public int connectionID { get; set; }

        public bool finalStation { get; set; }

        public bool isAbroad { get; set; }
    }
}
