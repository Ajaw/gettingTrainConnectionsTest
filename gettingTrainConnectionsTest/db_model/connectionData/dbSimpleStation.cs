﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.connectionData
{
    class dbSimpleStation
    {

        public int id { get; set; }

        public int? connectedStationID { get; set; }

        public int? motherStationID { get; set; }

        public int? previousStationID { get; set; }


    }
}
