﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.connectionData
{
    class ArrivalHours
    {
        public int ID { get; set; }

        public virtual dbConnectionToHour connectionToHour { get; set; }
        public int connectionToHourId { get; set; }

        [ForeignKey("newStationID")]
        public virtual dbStation realStation { get; set; }
        public int newStationID { get; set; }

        public virtual dbConnectedStation connectedStation { get; set; }

        public DateTime arrivalTime { get; set; }
        public DateTime departureTime { get; set; }
        public string platform { get; set; }
        public string track { get; set; }
    }
}
