﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gettingTrainConnectionsTest.db_model.connectionData;
namespace gettingTrainConnectionsTest.db_model
{
    class dbConnection
    {
        public dbConnection()
        {
            StopsOnRoute = new List<dbStationsOnConnection>();

            periodicConnectionList = new List<dbConnectionToHour>();
        }

        public int ID { get; set; }        

        public virtual ICollection<dbStationsOnConnection> StopsOnRoute { get; set; }

        public virtual ICollection<dbConnectionToHour> periodicConnectionList { get; set; }


    }
}
