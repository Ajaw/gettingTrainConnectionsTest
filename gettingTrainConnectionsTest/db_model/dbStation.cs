﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gettingTrainConnectionsTest.db_model.connectionData;
namespace gettingTrainConnectionsTest.db_model
{
    class dbStation
    {
        public dbStation()
        {

        }

        public dbStation(string stationName)
        {
            this.stationName = stationName;
        }

        public int ID { get; set; }
        public string stationName { get; set; }

        public double X { get; set; }
        public double Y { get; set; }


        public virtual ICollection<ArrivalHours> arrivalHoursOnStation { get; set; }

    }
}
