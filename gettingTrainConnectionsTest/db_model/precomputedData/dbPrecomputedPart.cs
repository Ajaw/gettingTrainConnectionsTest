﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.precomputedData
{
    class dbPrecomputedPart
    {

        public int id { get; set; }

        public virtual ICollection<dbPrecomputedSimpleStop> routeBluePrint { get; set; }

        public virtual ICollection<dbPrecomputedConnection> myConnections { get; set; }

    }
}
