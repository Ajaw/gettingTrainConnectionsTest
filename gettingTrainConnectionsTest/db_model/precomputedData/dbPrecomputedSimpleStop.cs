﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.precomputedData
{
    class dbPrecomputedSimpleStop
    {

        public int id { get; set; }

        [ForeignKey("precomputedPartID")]
        public virtual dbPrecomputedPart precomputedConnection { get; set; }
        public int precomputedPartID { get; set; }


        [ForeignKey("stationID")]
        public virtual dbStation station { get; set; }
        public int stationID { get; set; }

    }
}
