﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gettingTrainConnectionsTest.db_model.connectionData;
using System.ComponentModel.DataAnnotations.Schema;

namespace gettingTrainConnectionsTest.db_model.precomputedData
{
    class dbPrecomputedStop
    {

        public int id { get; set; }

        [ForeignKey("precomputedConnectionID")]
        public virtual dbPrecomputedConnection precomputedConnection { get; set; }
        public int precomputedConnectionID { get; set; }

        [ForeignKey("connectedStationID")]
        public virtual dbConnectedStation connectedStation { get; set; }
        public int connectedStationID { get; set; }

    }
}
