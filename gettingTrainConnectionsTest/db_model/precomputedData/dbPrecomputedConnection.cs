﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.db_model.precomputedData
{
    class dbPrecomputedConnection
    {

        public int id { get; set; }

        [ForeignKey("precomputedPartID")]
        public virtual dbPrecomputedPart Part { get; set; }

        public int precomputedPartID { get; set; }

    }
}
