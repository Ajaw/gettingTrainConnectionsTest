﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MySql.Data.Entity;
using gettingTrainConnectionsTest.db_model.connectionData;
using gettingTrainConnectionsTest.db_model.precomputedData;
namespace gettingTrainConnectionsTest.db_model
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
     class TrainDbContext : DbContext
    {
        public TrainDbContext() : base("trainDb")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
        }

        public DbSet<dbConnection> Connections { get; set; }

        public DbSet<dbStation> Stations { get; set; }

        public DbSet<dbStationsOnConnection> Stops { get; set; }

        public DbSet<ArrivalHours> StopHours { get; set; }        

        public DbSet<dbConnectionToHour> StopsHoursToConnection { get; set; }

        public DbSet<dbConnectedStation> ConnectedStationsStructure { get; set; }

        public DbSet<dbSimpleStation> SimpleStationsStructure { get; set; }

        public DbSet<dbPrecomputedConnection> PrecomputedConnections { get; set; }

        public DbSet<dbPrecomputedSimpleStop> PrecomputedSimpleStops { get; set; }

        public DbSet<dbPrecomputedStop> PrecomputedStops { get; set; }

        public DbSet<dbPrecomputedPart> Parts { get; set; }

        public DbSet<dbConnectionMapping> ConnectionMapping { get; set; }

        public DbSet<dbConnectionAvailability> ConnectionAvalibity { get; set; }

        public DbSet<dbScheduleVersion> ScheduleVersion { get; set; }

    }
}
