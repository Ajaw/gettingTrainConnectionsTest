﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Collections;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using gettingTrainConnectionsTest.model;
using OpenQA.Selenium.Interactions.Internal;
using gettingTrainConnectionsTest.db_model;
using gettingTrainConnectionsTest.controller;
namespace gettingTrainConnectionsTest
{
    public class selenium
    {
        public EventHandler logEvent;

        protected IWebDriver driver;





        public List<string> connectionLinks { get; set; }


        public List<station>  stations { get; set; }

        public List<connection> connections { get; set; }

        public string Info { get; set; }

        public int categoryId { get; set; }

        public string currentUrl { get; set; }

        public bool isSingleItem { get; set; }

        private bool isLicensed;

        public string searchedDate { get; set; }

        public  selenium()
        {
            //Progress(0, 1);
            // FirefoxProfile myprofile = new FirefoxProfile();\
            string pathToCurrentUserProfiles = Environment.ExpandEnvironmentVariables("%APPDATA%") + @"\Mozilla\Firefox\Profiles";
            string[] pathsToProfiles = Directory.GetDirectories(pathToCurrentUserProfiles, "*.default*", SearchOption.TopDirectoryOnly);
            if (pathsToProfiles.Length != 0)
            {


                FirefoxProfile profile = new FirefoxProfile(pathsToProfiles[0]);
                driver = new FirefoxDriver(profile);
            }
            else
            {
                driver = new FirefoxDriver();
            }

            connections = new List<connection>();
            stations = new List<station>();
            connectionLinks = new List<string>();

            //Licence_Check check = new Licence_Check();
            //this.isLicensed = check.isLicensed;
            this.isLicensed = true;
        }

        public void  Initialize(string url)
        {

            driver.Navigate().GoToUrl(url);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("katalog-item")));
            int page_number = Int32.Parse(driver.FindElement(By.ClassName("k-pager-last")).GetAttribute("data-page"));

            var current_page = Int32.Parse(driver.FindElement(By.ClassName("k-state-selected")).Text);
            for (int i = 2; i <= 1+1; i++)
            {
                var links = driver.FindElements(By.ClassName("k-link"));
                // var next_url = links.Where(a => a.GetAttribute("data-page") == i.ToString()).First().GetAttribute("href");
                var next_url = "https://portalpasazera.pl/KatalogiPolaczen?przewoznik=koleje-%C5%9Bl%C4%85skie&p=" + i;
                var elements = driver.FindElements(By.ClassName("katalog-item"));
               // var item = elements.First();
                foreach (var item in elements)
                {

                    var link = item.FindElement(By.XPath("parent::*"));
                    var link_to_go = link.GetAttribute("href");
                    connections.Add(new connection(item.Text, link_to_go));

                    connectionLinks.Add(link_to_go);
                    
                }
                if (i!=page_number+1)
                {
                    driver.Navigate().GoToUrl(next_url);
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("katalog-item")));
                }
             
            }
            driver.FindElement(By.Id("cookiesBarClose")).Click();
            Console.WriteLine("test");
            extractConnections();
            Console.WriteLine();
            populateHours();

        }

        private void extractConnections()
        {
            ConnectionController _contr = new ConnectionController();
            StationController _stat_contr = new StationController();
            
            foreach (var item in connections)
            {
                driver.Navigate().GoToUrl(item.url);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                List<station> temp_list = new List<station>();
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("katalog-item")));
                var elemnets =  driver.FindElements(By.CssSelector("span[class='katalog-pointer k-link']"));
                foreach (var station in elemnets)
                {
                    temp_list.Add(new station(item.url, station.Text));
                    if (!(_stat_contr.checkIfStationInDb(station.Text)))
                    {
                        _stat_contr.addStationToDb(station.Text);
                    }
                
                }
                item.stationOnRoute = temp_list;
                if (_contr.getConnectionByStationList(item) == null)
                {
                    item.connectionDbId = _contr.addConnectionToDb(item);
                }
                else
                {
                    item.connectionDbId = _contr.getConnectionByStationList(item).ID;
                }
            }
            
            
            
        }

        private void populateHours()
        {
            

            foreach (var item in connections)
            {
                try
                {

           
                  WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                driver.Navigate().GoToUrl("https://portalpasazera.pl/");
                driver.Navigate().Refresh();
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("k-button-search")));
                  

                    var inputs = driver.FindElements(By.TagName("input"));
                    searchedDate  = inputs.Where(a => a.GetAttribute("name") == "ko_unique_3").FirstOrDefault().GetAttribute("value"); ;
                    var out_input = inputs.Where(a => a.GetAttribute("name") == "ko_unique_1_input").FirstOrDefault();
                var in_input = inputs.Where(a => a.GetAttribute("name") == "ko_unique_2_input").FirstOrDefault();
                var time = inputs.Where(a => a.GetAttribute("name") == "ko_unique_4").FirstOrDefault();
                var direct = inputs.Where(a => a.GetAttribute("name") == "preferenceDirectly").FirstOrDefault();
                direct.Click();
                var search_button = driver.FindElement(By.ClassName("k-button-search"));
                var first_station = item.stationOnRoute.First();
                var last_station = item.stationOnRoute.Last();
                out_input.Click();
                out_input.SendKeys(first_station.name);
                in_input.Click();
                in_input.SendKeys(last_station.name);
                time.Click();
                time.Clear();
                time.SendKeys("00:00");
                Actions builder = new Actions(driver);
                builder.MoveToElement(search_button, 180,20).DoubleClick().Perform();
                search_button.SendKeys(Keys.Return);
                RetClick(search_button);
               wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
             
               // wait.Until(ExpectedConditions.ElementIsVisible(By.Id("searchResults")));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("pager-footer")));
                    
                   //  var footer = driver.FindElement(By.ClassName("sterowanie-szukaj-pozniej"));
                    var footer = driver.FindElement(By.ClassName("pager-footer"));
                while (checkIfMoreNeeded())
                {
                    try
                    {
                        footer.Click();
                        wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("wskaznikWyszukiwania")));
                    }
                    catch (Exception)
                    {

                        break;
                    }
                    
                }

                    getConnectionOpen(item);



                }
                catch (Exception ex)
                {

                
                }
            }

         
        }

        private void getConnectionOpen(connection _connection)
        {
            var main_table = driver.FindElement(By.Id("searchResults")).FindElement(By.TagName("tbody"));
            var elements = main_table.FindElements(By.TagName("tr"));
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            stopsController contr = new stopsController();
            List<HourlyRoute> routesOnConnection = new List<HourlyRoute>();

            foreach (var item in elements)
            {

         
                try
                {
                   var date = item.FindElements(By.TagName("td"))[1].Text;
                    if (date != searchedDate)
                    {
                        break;
                    }
                driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 1));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("fakeCell")));
                    var fake_cell = item.FindElement(By.ClassName("fakeCell"));
                Actions action = new Actions(driver);
                  
                    action.MoveToElement(fake_cell).Build().Perform();
                var show = item.FindElement(By.ClassName("showRowMenu"));
                    wait.Until(ExpectedConditions.ElementToBeClickable(show));
                    action.Click(show).Build().Perform();

                   // IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

                    wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("showDetails")));
                    var details = item.FindElement(By.ClassName("showDetails"));
                    action.MoveToElement(details,10,20).Build().Perform();
                    details.Click();
                    var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                    waiter.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div[class='col more']")));
                    waiter.Until(ExpectedConditions.ElementIsVisible(By.ClassName("showMoreDetails")));
                    details = driver.FindElement(By.ClassName("showMoreDetails"));
               var expand = details.FindElement(By.CssSelector("div[class='col more']"));
                    
                     
                    expand.Click();
                

                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("wskaznikWyszukiwania")));
                details = driver.FindElement(By.ClassName("szczegoly"));
                    SingleConnectionCheck check = new SingleConnectionCheck(details);

                    HourlyRoute tempRoute = new HourlyRoute();
                    List<StopOnRoute> arrivalHours = new List<StopOnRoute>();
                    if (check.checkIfGood())
                    {
                       arrivalHours =  check.processData();
                        Console.WriteLine(arrivalHours.Count);
                    }
                    tempRoute.stopsOnRoute = arrivalHours;
                    tempRoute.dbConnectionID = _connection.connectionDbId;
                    
                routesOnConnection.Add(tempRoute);
               

                var table = details.FindElement(By.TagName("tbody"));
                var stuff  = table.FindElements(By.TagName("tr")).ToList();
                var to_close = driver.FindElements(By.ClassName("k-i-close"));               
                foreach (var button in to_close)
                {
                    try
                    {
                        button.Click();
                    }
                    catch (Exception)
                    {

                    }
                }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                  
                }


            }
            contr.checkStopHours(_connection, routesOnConnection);


        }

        private bool checkIfMoreNeeded()
        {
          var main_table =   driver.FindElement(By.Id("searchResults")).FindElement(By.TagName("tbody"));
            var elements = main_table.FindElements(By.TagName("tr"));
            bool firstRun = true;
            string temp_text="";
            foreach (var tr in elements)
            {
                var columns = tr.FindElements(By.TagName("td"));
                if (temp_text != columns[1].Text)
                {
                    if (!firstRun)
                    {
                        return false;
                    }
                    else
                        firstRun = false;
                    
                }
                temp_text = columns[1].Text;


            }
            return true;
        }

        public void RetClick(IWebElement element)
        {
            var clicked = false;
            var attempts = 0;
        
            while (!clicked & attempts++ <2)
            {
               
                try
                {
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                    wait.Until(driver => element.Enabled);

                    wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("k-button-search")));
                    element = driver.FindElement(By.ClassName("k-button-search"));
                    driver.SwitchTo().DefaultContent();
                    Actions action = new Actions(driver);
                    //action.ClickAndHold(element).Build().Perform();
                   
                    element.Click();
                    var point = element.Location;
                    var size = element.Size;
                    double x = point.X + size.Width / 2.0;
                    double y = point.Y + size.Height / 2.0;
                    Dictionary<string, double> args = new Dictionary<string, double>();
                    args.Add("x", x);
                    args.Add("y", y);
                    IJavaScriptExecutor ex = (IJavaScriptExecutor)driver;
                    //  ex.ExecuteScript("mobile: tap", args);
                    ex.ExecuteScript("$('.k-button-search').trigger('click');");
                    clicked = true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    clicked = false;
                }
            }
        }

        public void Destroy()
        {
            driver.Quit();

        }


   

    

        



    }
}

