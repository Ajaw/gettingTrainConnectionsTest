﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using gettingTrainConnectionsTest.model;
using OpenQA.Selenium.Interactions.Internal;
namespace gettingTrainConnectionsTest
{
    public class SingleConnectionCheck
    {
        public IWebElement data { get; set; }

        public SingleConnectionCheck(IWebElement _data)
        {
            data = _data;
        }

        public bool checkIfGood()
        {
            var details = data.FindElement(By.CssSelector("div[class='col details']"));
            var operatorName = details.FindElements(By.TagName("a"));
            foreach (var item in operatorName)
            {
                if (item.GetAttribute("data-bind") == "text: PrzewoznikNazwa")
                {
                    var name = item.Text;
                    if (name.Contains("Śląskie"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return false;
       
        }

        public List<StopOnRoute> processData()
        {
            Console.WriteLine("begin");
            var table = data.FindElement(By.Id("gridTrasa")).FindElement(By.TagName("table")).FindElement(By.TagName("tbody"));
            var rows = table.FindElements(By.TagName("tr"));
            string station;
            string arrival;
            string departure;
            string platform;
            string distance;
            double number_distance;
            DateTime arrival_time;
            DateTime departure_time;
           
            List<StopOnRoute> hoursList = new List<StopOnRoute>();
            foreach (var item in rows)
            {
                StopOnRoute temp_stop = new StopOnRoute();
                var columns = item.FindElements(By.TagName("td"));
                station = columns[0].Text;                
                arrival = processTime(columns[1]);
                departure = processTime(columns[2]);
                platform = columns[3].Text;
                distance = columns[4].Text;
                DateTime.TryParse(arrival, out arrival_time);
                DateTime.TryParse(departure, out departure_time);
                Double.TryParse(distance, out number_distance);
                temp_stop.arrivalTime = arrival_time;
                temp_stop.departureTime = departure_time;
                temp_stop.distance = number_distance;
                temp_stop.stationName = station;
                temp_stop.platform = platform;
                hoursList.Add(temp_stop);        
            }          
            Console.WriteLine("end");
            return hoursList;
        }

        public string processTime(IWebElement element)
        {
            if (element.Text == String.Empty)
            {


                Console.WriteLine("empty end process");
                return String.Empty;
            }
            Console.WriteLine("begin time process");
            var spans = element.FindElements(By.TagName("span"));
            string className;
            foreach (var item in spans)
                {
                     className = item.GetAttribute("class");
                if (className == String.Empty)
                {
                    Console.WriteLine("end time process");
                    return item.Text;
                }
                if (className == "oldTime")
                    {
                        Console.WriteLine("end time process");
                        return item.Text;
                    }
                
                }
            return String.Empty;
        }

    }
}
