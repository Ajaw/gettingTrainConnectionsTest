﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.model
{
    public class StopOnRoute
    {
        public DateTime arrivalTime { get; set; }
        public DateTime departureTime { get; set; }
        public string stationName { get; set; }
        public double distance { get; set; }
        public string platform { get; set; }
    }
}
