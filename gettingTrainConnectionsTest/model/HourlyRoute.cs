﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.model
{
    public class HourlyRoute
    {
        public bool alreadyInDB { get; set; }
        public List<StopOnRoute> stopsOnRoute { get; set; }
        public bool onWeekend { get; set; }
        public int dbConnectionID { get; set; }
        public int dbConnectionHourID { get; set; }
    }
}
