﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gettingTrainConnectionsTest.db_model;
namespace gettingTrainConnectionsTest.model
{
    public class connection
    {
        public connection(string name, string url)
        {
            connectionName = name;
            this.url = url;
        }

        public int id { get; set; }
        public string connectionName { get; set; }
        public List<station> stationOnRoute { get; set; }
        public int connectionDbId { get; set; }
        public string url { get; set; }
    }
}
