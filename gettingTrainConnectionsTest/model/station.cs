﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gettingTrainConnectionsTest.model
{
    public class station
    {
        public station(string url, string name)
        {
            this.url = url;
            this.name = name;
        }
        public int stationID { get; set; }
        public string url { get; set; }
        public string name { get; set; }
    }
}
