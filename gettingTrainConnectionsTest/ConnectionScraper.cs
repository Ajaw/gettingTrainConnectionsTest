﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using gettingTrainConnectionsTest.model;
using OpenQA.Selenium.Interactions.Internal;
namespace gettingTrainConnectionsTest
{
    public class ConnectionScraper
    {
        IWebDriver driver;

        public ConnectionScraper(IWebDriver _driver)
        {
            driver = _driver;
            var fake_cell = driver.FindElement(By.ClassName("fakeCell"));
            Actions action = new Actions(driver);
            action.MoveToElement(fake_cell).Build().Perform();
            var show = driver.FindElement(By.ClassName("showRowMenu"));
            action.Click(show).Build().Perform();
            var details = driver.FindElement(By.ClassName("showDetails"));
            details.Click();
        }
    }
}
