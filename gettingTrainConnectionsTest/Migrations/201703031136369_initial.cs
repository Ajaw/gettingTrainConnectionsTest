namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dbConnections",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.dbConnectionToHours",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        connectionID = c.Int(nullable: false),
                        onWeekendConnection = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.dbConnections", t => t.connectionID, cascadeDelete: true)
                .Index(t => t.connectionID);
            
            CreateTable(
                "dbo.ArrivalHours",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        connectionToHourId = c.Int(nullable: false),
                        arrivalTime = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.dbConnectionToHours", t => t.connectionToHourId, cascadeDelete: true)
                .Index(t => t.connectionToHourId);
            
            CreateTable(
                "dbo.dbStationsOnConnections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        connectionID = c.Int(nullable: false),
                        stationID = c.Int(nullable: false),
                        stationNumberOnRoute = c.Int(nullable: false),
                        distanceFromLastStation = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dbConnections", t => t.connectionID, cascadeDelete: true)
                .ForeignKey("dbo.dbStations", t => t.stationID, cascadeDelete: true)
                .Index(t => t.connectionID)
                .Index(t => t.stationID);
            
            CreateTable(
                "dbo.dbStations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        stationName = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dbStationsOnConnections", "stationID", "dbo.dbStations");
            DropForeignKey("dbo.dbStationsOnConnections", "connectionID", "dbo.dbConnections");
            DropForeignKey("dbo.dbConnectionToHours", "connectionID", "dbo.dbConnections");
            DropForeignKey("dbo.ArrivalHours", "connectionToHourId", "dbo.dbConnectionToHours");
            DropIndex("dbo.dbStationsOnConnections", new[] { "stationID" });
            DropIndex("dbo.dbStationsOnConnections", new[] { "connectionID" });
            DropIndex("dbo.ArrivalHours", new[] { "connectionToHourId" });
            DropIndex("dbo.dbConnectionToHours", new[] { "connectionID" });
            DropTable("dbo.dbStations");
            DropTable("dbo.dbStationsOnConnections");
            DropTable("dbo.ArrivalHours");
            DropTable("dbo.dbConnectionToHours");
            DropTable("dbo.dbConnections");
        }
    }
}
