namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dbPrecomputedParts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.dbPrecomputedConnections",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        precomputedPartID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.dbPrecomputedParts", t => t.precomputedPartID, cascadeDelete: true)
                .Index(t => t.precomputedPartID);
            
            CreateTable(
                "dbo.dbPrecomputedSimpleStops",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        precomputedPartID = c.Int(nullable: false),
                        stationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.dbPrecomputedParts", t => t.precomputedPartID, cascadeDelete: true)
                .ForeignKey("dbo.dbStations", t => t.stationID, cascadeDelete: true)
                .Index(t => t.precomputedPartID)
                .Index(t => t.stationID);
            
            CreateTable(
                "dbo.dbPrecomputedStops",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        precomputedConnectionID = c.Int(nullable: false),
                        connectedStationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.dbConnectedStations", t => t.connectedStationID, cascadeDelete: true)
                .ForeignKey("dbo.dbPrecomputedConnections", t => t.precomputedConnectionID, cascadeDelete: true)
                .Index(t => t.precomputedConnectionID)
                .Index(t => t.connectedStationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dbPrecomputedStops", "precomputedConnectionID", "dbo.dbPrecomputedConnections");
            DropForeignKey("dbo.dbPrecomputedStops", "connectedStationID", "dbo.dbConnectedStations");
            DropForeignKey("dbo.dbPrecomputedSimpleStops", "stationID", "dbo.dbStations");
            DropForeignKey("dbo.dbPrecomputedSimpleStops", "precomputedPartID", "dbo.dbPrecomputedParts");
            DropForeignKey("dbo.dbPrecomputedConnections", "precomputedPartID", "dbo.dbPrecomputedParts");
            DropIndex("dbo.dbPrecomputedStops", new[] { "connectedStationID" });
            DropIndex("dbo.dbPrecomputedStops", new[] { "precomputedConnectionID" });
            DropIndex("dbo.dbPrecomputedSimpleStops", new[] { "stationID" });
            DropIndex("dbo.dbPrecomputedSimpleStops", new[] { "precomputedPartID" });
            DropIndex("dbo.dbPrecomputedConnections", new[] { "precomputedPartID" });
            DropTable("dbo.dbPrecomputedStops");
            DropTable("dbo.dbPrecomputedSimpleStops");
            DropTable("dbo.dbPrecomputedConnections");
            DropTable("dbo.dbPrecomputedParts");
        }
    }
}
