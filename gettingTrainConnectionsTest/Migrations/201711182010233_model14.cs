namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model14 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ArrivalHours", "connectedStation_id", c => c.Int());
            CreateIndex("dbo.ArrivalHours", "connectedStation_id");
            AddForeignKey("dbo.ArrivalHours", "connectedStation_id", "dbo.dbConnectedStations", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ArrivalHours", "connectedStation_id", "dbo.dbConnectedStations");
            DropIndex("dbo.ArrivalHours", new[] { "connectedStation_id" });
            DropColumn("dbo.ArrivalHours", "connectedStation_id");
        }
    }
}
