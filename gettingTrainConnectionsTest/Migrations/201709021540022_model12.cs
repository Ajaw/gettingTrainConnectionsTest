namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model12 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dbConnectionAvailabilities",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        beggingDate = c.DateTime(nullable: false, precision: 0),
                        endDate = c.DateTime(nullable: false, precision: 0),
                        ignoreWeekDays = c.Boolean(nullable: false),
                        mapping_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.dbConnectionMappings", t => t.mapping_id)
                .Index(t => t.mapping_id);
            
            CreateTable(
                "dbo.dbConnectionMappings",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        monday = c.Boolean(nullable: false),
                        tuesday = c.Boolean(nullable: false),
                        wenesday = c.Boolean(nullable: false),
                        thursday = c.Boolean(nullable: false),
                        friday = c.Boolean(nullable: false),
                        saturday = c.Boolean(nullable: false),
                        sunday = c.Boolean(nullable: false),
                        connection_number = c.String(unicode: false),
                        was_processed = c.Boolean(nullable: false),
                        connection_ID = c.Int(),
                        connectionToHour_ID = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.dbConnections", t => t.connection_ID)
                .ForeignKey("dbo.dbConnectionToHours", t => t.connectionToHour_ID)
                .Index(t => t.connection_ID)
                .Index(t => t.connectionToHour_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dbConnectionMappings", "connectionToHour_ID", "dbo.dbConnectionToHours");
            DropForeignKey("dbo.dbConnectionAvailabilities", "mapping_id", "dbo.dbConnectionMappings");
            DropForeignKey("dbo.dbConnectionMappings", "connection_ID", "dbo.dbConnections");
            DropIndex("dbo.dbConnectionMappings", new[] { "connectionToHour_ID" });
            DropIndex("dbo.dbConnectionMappings", new[] { "connection_ID" });
            DropIndex("dbo.dbConnectionAvailabilities", new[] { "mapping_id" });
            DropTable("dbo.dbConnectionMappings");
            DropTable("dbo.dbConnectionAvailabilities");
        }
    }
}
