namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model9 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dbConnectedStations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        motherStationID = c.Int(nullable: false),
                        connectedStationID = c.Int(nullable: false),
                        previousStationID = c.Int(nullable: false),
                        connectionID = c.Int(nullable: false),
                        finalStation = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.dbSimpleStations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        connectedStationID = c.Int(nullable: false),
                        motherStationID = c.Int(nullable: false),
                        previousStationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.dbSimpleStations");
            DropTable("dbo.dbConnectedStations");
        }
    }
}
