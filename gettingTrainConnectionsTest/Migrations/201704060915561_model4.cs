namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dbConnectedStations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        connectedStationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.dbStations", t => t.connectedStationID, cascadeDelete: true)
                .Index(t => t.connectedStationID);
            
            AddColumn("dbo.ArrivalHours", "newStationID", c => c.Int(nullable: false));
            CreateIndex("dbo.ArrivalHours", "newStationID");
            AddForeignKey("dbo.ArrivalHours", "newStationID", "dbo.dbStations", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dbConnectedStations", "connectedStationID", "dbo.dbStations");
            DropForeignKey("dbo.ArrivalHours", "newStationID", "dbo.dbStations");
            DropIndex("dbo.ArrivalHours", new[] { "newStationID" });
            DropIndex("dbo.dbConnectedStations", new[] { "connectedStationID" });
            DropColumn("dbo.ArrivalHours", "newStationID");
            DropTable("dbo.dbConnectedStations");
        }
    }
}
