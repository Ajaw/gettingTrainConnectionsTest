namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.dbConnectedStations", "stationConnectionID", c => c.Int(nullable: false));
            AddColumn("dbo.dbConnectionToHours", "trainLabel", c => c.String(unicode: false));
            CreateIndex("dbo.dbConnectedStations", "stationConnectionID");
            AddForeignKey("dbo.dbConnectedStations", "stationConnectionID", "dbo.dbConnections", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dbConnectedStations", "stationConnectionID", "dbo.dbConnections");
            DropIndex("dbo.dbConnectedStations", new[] { "stationConnectionID" });
            DropColumn("dbo.dbConnectionToHours", "trainLabel");
            DropColumn("dbo.dbConnectedStations", "stationConnectionID");
        }
    }
}
