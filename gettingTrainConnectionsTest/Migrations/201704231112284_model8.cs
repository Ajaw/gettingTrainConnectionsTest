namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model8 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ArrivalHours", "track", c => c.String(unicode: false));
            AddColumn("dbo.dbStations", "X", c => c.Double(nullable: false));
            AddColumn("dbo.dbStations", "Y", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.dbStations", "Y");
            DropColumn("dbo.dbStations", "X");
            DropColumn("dbo.ArrivalHours", "track");
        }
    }
}
