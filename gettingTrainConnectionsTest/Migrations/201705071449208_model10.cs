namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model10 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.dbSimpleStations", "connectedStationID", c => c.Int());
            AlterColumn("dbo.dbSimpleStations", "motherStationID", c => c.Int());
            AlterColumn("dbo.dbSimpleStations", "previousStationID", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.dbSimpleStations", "previousStationID", c => c.Int(nullable: false));
            AlterColumn("dbo.dbSimpleStations", "motherStationID", c => c.Int(nullable: false));
            AlterColumn("dbo.dbSimpleStations", "connectedStationID", c => c.Int(nullable: false));
        }
    }
}
