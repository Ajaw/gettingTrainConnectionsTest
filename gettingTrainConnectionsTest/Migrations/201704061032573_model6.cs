namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model6 : DbMigration
    {
        public override void Up()
        {
     
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.dbConnectedStations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        connectedStationID = c.Int(nullable: false),
                        stationConnectionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateIndex("dbo.dbConnectedStations", "stationConnectionID");
            CreateIndex("dbo.dbConnectedStations", "connectedStationID");
            AddForeignKey("dbo.dbConnectedStations", "stationConnectionID", "dbo.dbConnections", "ID", cascadeDelete: true);
            AddForeignKey("dbo.dbConnectedStations", "connectedStationID", "dbo.dbStations", "ID", cascadeDelete: true);
        }
    }
}
