namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model15 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dbScheduleVersions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        dateFrom = c.DateTime(nullable: false, precision: 0),
                        dateTo = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.dbConnectedStations", "isAbroad", c => c.Boolean(nullable: false));
            AddColumn("dbo.dbConnectionMappings", "version_id", c => c.Int());
            AddColumn("dbo.dbConnectionToHours", "version_id", c => c.Int());
            CreateIndex("dbo.dbConnectionMappings", "version_id");
            CreateIndex("dbo.dbConnectionToHours", "version_id");
            AddForeignKey("dbo.dbConnectionToHours", "version_id", "dbo.dbScheduleVersions", "id");
            AddForeignKey("dbo.dbConnectionMappings", "version_id", "dbo.dbScheduleVersions", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dbConnectionMappings", "version_id", "dbo.dbScheduleVersions");
            DropForeignKey("dbo.dbConnectionToHours", "version_id", "dbo.dbScheduleVersions");
            DropIndex("dbo.dbConnectionToHours", new[] { "version_id" });
            DropIndex("dbo.dbConnectionMappings", new[] { "version_id" });
            DropColumn("dbo.dbConnectionToHours", "version_id");
            DropColumn("dbo.dbConnectionMappings", "version_id");
            DropColumn("dbo.dbConnectedStations", "isAbroad");
            DropTable("dbo.dbScheduleVersions");
        }
    }
}
