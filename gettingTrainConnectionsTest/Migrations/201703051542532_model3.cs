namespace gettingTrainConnectionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class model3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ArrivalHours", "departureTime", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.ArrivalHours", "platform", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ArrivalHours", "platform");
            DropColumn("dbo.ArrivalHours", "departureTime");
        }
    }
}
